# Automatic Speech Recognition (Wav2Vec 2.0 based)

In this repository, we use [wav2vec 2.0](https://arxiv.org/abs/2006.11477) which recently has a code release in [fairseq](https://github.com/pytorch/fairseq) in August 2020. The [pretrained models](https://github.com/pytorch/fairseq/tree/master/examples/wav2vec) are also available after release and possibly more models will be release later.

# Installation

Based on this [issue](https://github.com/pytorch/fairseq/issues/2651) on github.

Install required dependencies.

```bash
# Install python libraries
pip install soundfile
pip install torchaudio
pip install sentencepiece

# Update apt-get & Install soundfile
apt-get update \
&& apt-get upgrade -y \
&& apt-get install -y \
&& apt-get -y install apt-utils gcc libpq-dev libsndfile-dev

# Install kenlm
mkdir external_lib
cd external_lib

sudo apt install build-essential cmake libboost-system-dev libboost-thread-dev libboost-program-options-dev libboost-test-dev libeigen3-dev zlib1g-dev libbz2-dev liblzma-dev
git clone https://github.com/kpu/kenlm.git
cd kenlm
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DKENLM_MAX_ORDER=20 -DCMAKE_POSITION_INDEPENDENT_CODE=ON
make -j 16
export KENLM_ROOT_DIR=$ABSOLUTE_PATH'/external_lib/kenlm/'
cd ../..

# Install Additional Dependencies (ATLAS, OpenBLAS, Accelerate, Intel MKL)
apt-get install libsndfile1-dev libopenblas-dev libfftw3-dev libgflags-dev libgoogle-glog-dev

# Install wav2letter
git clone -b v0.2 https://github.com/facebookresearch/wav2letter.git
cd wav2letter/bindings/python
pip install -e .
cd ../../..
```

Clone `fairseq` repository and install it. If `fairseq` is already on your system just clone the repo without installing it might works since we clone it here so it can be easier to work with. **Do not install the package from pypi** since the package there is outdated at the time of writing this document.

```bash
git clone  https://github.com/pytorch/fairseq.git
cd fairseq
pip install --editable ./
cd ..
```

The docker file is also provided.

```bash
sudo docker build -t zincorca/wav2vec2.0_asr:dev - < Dockerfile 
```

```bash
sudo docker run --rm -it --gpus all -v $PWD/data:/usr/src/app/data -v $PWD/scripts:/usr/src/app/scripts zincorca/wav2vec2.0_asr:dev
```

In case of sharing dataset with different projects, you can use `sudo mount --bind source_dataset target_mountpoint` to bind `source_dataset` to specified folder. This method will make docker see `source_dataset` at `target_mountpoint` as well. The less intrusive way without using `mount` is to make symlink instead `ln -s source_dataset target_mountpoint`. However, docker will not be able to see the data. Or you can just mount the data in the same directory which symlink pointed to that might work.

*There might be some error message later in training step it has to do with some missing class but this should not affect our use case*

# Preparing data

Download librespeech dataset from [here](http://www.openslr.org/12).

```bash
$ ls -1 data/input/LibriSpeech/
BOOKS.TXT
CHAPTERS.TXT
LICENSE.TXT
README.TXT
SPEAKERS.TXT
train-clean-100
```

Create manifest file.

```bash
ext=flac
valid=0.9972
audio_path=data/input/LibriSpeech/
manifest_path=data/output/manifest
mkdir -p $manifest_path
python3 fairseq/examples/wav2vec/wav2vec_manifest.py $audio_path  --dest $manifest_path  --ext $ext --valid-percent $valid
```

The valid control how the data split in this case `99.72 %` of data is use as validation.

Make labels.

```bash
python3 fairseq/examples/wav2vec/libri_labels.py $manifest_path/train.tsv --output-dir $manifest_path --output-name train
python3 fairseq/examples/wav2vec/libri_labels.py $manifest_path/valid.tsv --output-dir $manifest_path --output-name valid
```

Download dictionary from here.

```bash
curl https://dl.fbaipublicfiles.com/fairseq/wav2vec/dict.ltr.txt > $manifest_path/dict.ltr.txt
```

Dictionary look some thing like this the `|` symbol is use to represent space. It contain each letter represent in the text files.

```bash
$ cat $manifest_path/dict.ltr.txt
| 94802
E 51860
T 38431
A 33152
...
```

# Training

Most of the commands below are adapted from their [example](https://github.com/pytorch/fairseq/tree/master/examples/wav2vec). Those commands are used on system with huge VRAM and lot of GPU. But our current goal is to run it on small scale be familiar with the code.

The commands are tested on a system with GTX-1070 8GB.

# Unsupervised training

This step will train a model in unsupervised manner. We will have pretrained model after we are done with this step which can be later be finetuned on labeled data. We will skip this step and use downloaded pretrained network instead.

```bash
pretrain_model=data/input/downloaded_pretrain/wav2vec_small.pt
mkdir -p $(dirname $pretrain_model)
curl https://dl.fbaipublicfiles.com/fairseq/wav2vec/wav2vec_small.pt > $pretrain_model
```

# Finetuning

Finetune pretrain model from pretrained network.

```bash
finetuned_model_path=data/output/finetuned_model
python3 fairseq/train.py $manifest_path --save-dir $finetuned_model_path \
--post-process letter --valid-subset valid --train-subset train --no-epoch-checkpoints --num-workers 4 \
--max-update 10000 --sentence-avg --task audio_pretraining --arch wav2vec_ctc --w2v-path $pretrain_model \
--labels ltr --apply-mask --mask-selection static --mask-other 0 --mask-length 10 --mask-prob 0.5 --layerdrop 0.1 \
--mask-channel-selection static --mask-channel-other 0 --mask-channel-length 64 --mask-channel-prob 0.5 --zero-infinity \
--feature-grad-mult 0.0 --freeze-finetune-updates 0 --optimizer adam \
--adam-betas '(0.9, 0.98)' --adam-eps 1e-08 --lr 2e-05 --lr-scheduler tri_stage --warmup-steps 8000 --hold-steps 32000 \
--decay-steps 40000 --final-lr-scale 0.05 --final-dropout 0.0 --dropout 0.0 --activation-dropout 0.1 --criterion ctc \
--attention-dropout 0.0 --max-tokens 400000 --seed 2337 --log-format json --log-interval 500 --ddp-backend no_c10d \
--disable-validation --save-interval 25
```

Here are some arguments that we changed or important.

| Arguments | Description | Remark |
| --------- | ----------- | ------ |
| valid-subset | name of manifest file use for validation | |
| train-subset | name of manifest file use for training | |
| max-update | number of iterations | |
| freeze-finetune-updates | number of updates step with freezed weights on pretrained model | setting this to `0` mean unfreeze all weights from start |
| max-tokens | control maximum token which can be fit in each iteration | this dictate how much memory use lower this for lower memory consumption |
| save-interval | number of epoch before saving checkpoint (epoch % interval == 0) | |
| disable-validation | disable validation | make training a lot faster on small slice of training data |

# Evaluation

Evaluation is done without LM for now.

```bash
result_path=data/output/result
python3 fairseq/examples/speech_recognition/infer.py $manifest_path --task audio_pretraining \
--nbest 1 --path $finetuned_model_path/checkpoint_last.pt --gen-subset valid --results-path $result_path --w2l-decoder viterbi \
--lm-weight 2 --word-score -1 --sil-weight 0 --criterion ctc --labels ltr --max-tokens 400000
```

We can also use `sclite` to evaluate result.

```bash
sctk sclite -h $result_path/hypo.word-checkpoint_last.pt-valid.txt -r $result_path/ref.word-checkpoint_last.pt-valid.txt -i wsj
```

# Example

Example `scripts/run.sh` is provided so you can use this as a starting point.

The `data` folder should have `data/input/LibriSpeech/` which LibriSpeech dataset reside for it to work.

```bash
sudo docker run --rm -it --gpus all -v $PWD/data:/usr/src/app/data -v $PWD/scripts:/usr/src/app/scripts zincorca/wav2vec2.0_asr:dev
./scripts/run.sh
```
