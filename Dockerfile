FROM nvidia/cuda:10.2-devel-ubuntu18.04

ARG PROJ_DIR=/usr/src/app/

RUN apt-get update && apt-get upgrade -y
RUN apt-get install python3-pip -y
RUN pip3 install torch
RUN pip3 install soundfile
RUN pip3 install torchaudio
RUN pip3 install -U pip
RUN pip3 install sentencepiece
RUN DEBIAN_FRONTEND=noninteractive apt-get install apt-utils gcc libpq-dev libsndfile-dev -y
RUN apt-get install -y git

WORKDIR ${PROJ_DIR}/external_lib

RUN apt-get install -y build-essential cmake libboost-system-dev libboost-thread-dev libboost-program-options-dev libboost-test-dev libeigen3-dev zlib1g-dev libbz2-dev liblzma-dev

RUN git clone https://github.com/kpu/kenlm.git --depth=1 && cd kenlm && \
    mkdir -p build && cd build && \
    cmake .. -DCMAKE_BUILD_TYPE=Release -DKENLM_MAX_ORDER=20 -DCMAKE_POSITION_INDEPENDENT_CODE=ON && \
    make -j $(nproc)

ENV KENLM_ROOT_DIR ${PROJ_DIR}/external_lib/kenlm

RUN apt-get install libsndfile1-dev libopenblas-dev libfftw3-dev libgflags-dev libgoogle-glog-dev -y
RUN pip3 install packaging
RUN git clone -b v0.2 https://github.com/facebookresearch/wav2letter.git --depth=1 && \
    cd wav2letter/bindings/python && \
    pip3 install -e .

WORKDIR ${PROJ_DIR}

RUN git clone  https://github.com/pytorch/fairseq.git && \
    cd fairseq && \
    pip install --editable ./

RUN apt-get install curl -y
RUN apt-get install sctk -y
