# Create variables
ext=flac
valid=0.9972
audio_path=data/input/LibriSpeech/
manifest_path=data/output/manifest
pretrain_model=data/input/downloaded_pretrain/wav2vec_small.pt
finetuned_model_path=data/output/finetuned_model
result_path=data/output/result

# Create manifest files
mkdir -p $manifest_path
python3 fairseq/examples/wav2vec/wav2vec_manifest.py $audio_path  --dest $manifest_path  --ext $ext --valid-percent $valid
python3 fairseq/examples/wav2vec/libri_labels.py $manifest_path/train.tsv --output-dir $manifest_path --output-name train
python3 fairseq/examples/wav2vec/libri_labels.py $manifest_path/valid.tsv --output-dir $manifest_path --output-name valid

# Grab a dictionary
if [ ! -f $manifest_path/dict.ltr.txt ]; then
    curl https://dl.fbaipublicfiles.com/fairseq/wav2vec/dict.ltr.txt > $manifest_path/dict.ltr.txt
else
    echo "Dictionary already exists."
fi

# Grab a pretrained model
mkdir -p $(dirname $pretrain_model)
if [ ! -f $pretrain_model ]; then
    curl https://dl.fbaipublicfiles.com/fairseq/wav2vec/wav2vec_small.pt > $pretrain_model
else
    echo "Pretrain model already exists."
fi

# Finetuning model
python3 fairseq/train.py $manifest_path --save-dir $finetuned_model_path \
--post-process letter --valid-subset valid --train-subset train --no-epoch-checkpoints --num-workers 4 \
--max-update 15000 --sentence-avg --task audio_pretraining --arch wav2vec_ctc --w2v-path $pretrain_model \
--labels ltr --apply-mask --mask-selection static --mask-other 0 --mask-length 10 --mask-prob 0.5 --layerdrop 0.1 \
--mask-channel-selection static --mask-channel-other 0 --mask-channel-length 64 --mask-channel-prob 0.5 --zero-infinity \
--feature-grad-mult 0.0 --freeze-finetune-updates 0 --optimizer adam \
--adam-betas '(0.9, 0.98)' --adam-eps 1e-08 --lr 2e-05 --lr-scheduler tri_stage --warmup-steps 8000 --hold-steps 32000 \
--decay-steps 40000 --final-lr-scale 0.05 --final-dropout 0.0 --dropout 0.0 --activation-dropout 0.1 --criterion ctc \
--attention-dropout 0.0 --max-tokens 400000 --seed 2337 --log-format json --log-interval 500 --ddp-backend no_c10d \
--disable-validation --save-interval 25

# Evaluating model
python3 fairseq/examples/speech_recognition/infer.py $manifest_path --task audio_pretraining \
--nbest 1 --path $finetuned_model_path/checkpoint_last.pt --gen-subset valid --results-path $result_path --w2l-decoder viterbi \
--lm-weight 2 --word-score -1 --sil-weight 0 --criterion ctc --labels ltr --max-tokens 400000

# Evaluating with sclite
sctk sclite -h $result_path/hypo.word-checkpoint_last.pt-valid.txt -r $result_path/ref.word-checkpoint_last.pt-valid.txt -i wsj
